#!/usr/bin/env python3
import sys
import zipfile
import logging
import io
import os
import subprocess
import requests
import time
import json
from datetime import datetime
from itertools import zip_longest
from cifsdk.exceptions import SubmissionFailed
from cifsdk.client.http import HTTP as Client


def grouper(n, iterable, fillvalue=None):
    "grouper(3, 'ABCDEFG', 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(fillvalue=fillvalue, *args)

def resolve_host_batch(host_list):
    """
    Using an async system call instead of adns...adns seem a bit sketchy to compile
    """

    cmd = ['adnshost', '--asynch', '-Fs']
    # Remove 'None' hosts
    host_list = [i for i in host_list if i]
    cmd.extend(host_list)
    p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE)
    output, err = p.communicate()
    rc = p.returncode
    return_items = []
    for item in (output.split(b"\n")):
        if item == b'':
            continue
        if b'CNAME' in item:
            continue
        try:
            name, record_type, proto, addr = item.split()
        except Exception as e:
            logging.error("Could not parse %s" % item)
            continue

        return_items.append(addr)

    return return_items
def resolve_hosts(host_list, parallel=50):
    return_items = []
    for batch in grouper(parallel, host_list):
        ips = resolve_host_batch(batch)
        return_items.extend(ips)
    return return_items


def ingest_alexa(limit=1000):
    '''
    # Stealing most of this part from Jesse - DS
    Given a limit, download and parse the Alexa Top 1 Million sites for domains
    :param limit: (int) Total number of sites to extract (up to 1 million)
    :return: (list) A unique list of names to be resolved
    '''

    # Prepare a timer and check sanity
    stime = time.time()
    assert limit < 1000001, "Limit must not be more than 1 million!"

    # Get the top 1 million sites
    url = 'http://s3.amazonaws.com/alexa-static/top-1m.csv.zip'
    try:
        r = requests.get(url, allow_redirects=True)
    except Exception as e:
        logging.error("Exception retrieving Alexa list: {0}".format(repr(e)))
        exit(1)

    # Report download time
    runtime = time.time() - stime
    logging.info("Download runtime: {0}".format(runtime))

    r.raise_for_status()

    # Store the names to look up
    names = list()

    zip_res = io.BytesIO(r.content)

    # This is a zip file
    with zipfile.ZipFile(zip_res, 'r') as zip_file:

        # We assume the first (and only) item in the zip is our file
        toplist = zip_file.namelist()[0]
        logging.debug("Found item {0} in zipfile".format(toplist))
        items_file = zip_file.open(toplist, 'r')
        items_file = io.TextIOWrapper(items_file)

        # iterate to the desired count and build name list
        logging.debug(
            "Iterating through {0} looking for names".format(toplist))
        for idx, row in enumerate(items_file):
            if idx >= limit:
                break
            rank, name = row.split(',')
            names.append(name.strip())
    logging.info("Collected {0} names for resolution.".format(len(names)))
    # Report timing
    runtime = time.time() - stime
    logging.info("Name Extraction runtime: {0}".format(runtime))
    return names


def submit_to_cif(data, host, ssl, token):
    logging.debug('Initializing Client instance to host={}, with ssl={}'.format(host, ssl))
    cli = Client(token=token,
                 remote=host,
                 verify_ssl=ssl)
    logging.info('Submitting indicator: {0}'.format(data))
    try:
        r = cli.indicators_create(json.dumps(data))
        logging.debug('Indicator submitted with id {}'.format(r))
        return True
    except (SubmissionFailed, Exception) as e:
        if isinstance(e, SubmissionFailed):
            logging.error("%s" % e)
            logging.error(
                'Submission failed due to authorization error; please correct your host/key, remove this container, and try again')
            return False
        else:
            logging.error('Error submitting indicator: {} {}'.format(type(e).__name__, e.args))
            return False

def main():
    hosts = (ingest_alexa(5))
    ips = resolve_hosts(hosts, 50)
    print("Got %s ips back" % len(ips))
    for ip in ips:
        print(ip)
        data = {
            "rdata": "%s" % ip,
            "tlp": "green",
            "confidence": 95,
            "provider": "stingar-alexa-bot",
            "group": "everyone", # <- 🤔
            "tags": ["whitelist"],
            "lasttime": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
        }
        logging.basicConfig(level=logging.DEBUG)
        submit_to_cif(data, host=os.environ['CIF_HOST'], ssl=True, token=os.environ['CIF_TOKEN'])
    return 0

if __name__ == "__main__":
    sys.exit(main())
