FROM python:3.7

RUN mkdir /code
    
COPY requirements.txt /code/
COPY run.py /code/

RUN apt -y update && \
    apt -y install adns-tools python3-pip && \
    pip3 install -r /code/requirements.txt

CMD /code/run.py
